#![warn(clippy::all)]

extern crate gtk;
extern crate gio;

// To import all needed traits.
use gtk::prelude::*;
use glib::clone;
use gio::prelude::*;

use gtk::{ApplicationWindow, Builder, Button, MessageDialog};

use std::env;
use std::io::Write;
use std::path::Path;

fn build_ui(app: &gtk::Application) {
    let builder = Builder::new_from_string(include_str!("window.glade"));

    let window: ApplicationWindow = builder.get_object(&"MainWindow").expect("Couldn't get MainWindow!");
    window.set_application(Some(app));

    let btn_hi: Button = builder.get_object("btnHi").expect("Could not get btnHi from interface :(");
    btn_hi.connect_clicked(clone!(@weak window => move |_| {
        
        let dlg = MessageDialog::new(Some(&window), gtk::DialogFlags::empty(), gtk::MessageType::Info,
            gtk::ButtonsType::Ok, &"I love you :3 💖"
        );
        dlg.run();
        dlg.destroy();


        let fc = gtk::FileChooserDialogBuilder::new();
        let fcd = fc.build();
        let ff = gtk::FileFilter::new();
        
        fcd.set_filter(&ff);
        fcd.set_title(&"I want to give you a gift! 🦈");
        fcd.set_action(gtk::FileChooserAction::SelectFolder);
        fcd.add_button(&"Save🦈", gtk::ResponseType::Ok);
        fcd.add_button(&"Cancel🔥", gtk::ResponseType::Cancel);

        fcd.run();

        let success = match fcd.get_filename() {
            Some(path) => {
                let saveto = Path::new(&path).join("shark.jpg");
                let savepath: &Path = Path::new(saveto.to_str().unwrap());
                println!("Path is {}", &savepath.to_str().unwrap());

                if savepath.exists() {
                    println!("NOT overwriting files *smh* 🤦‍♀");
                } else {
                    let file = std::fs::File::create(&savepath).expect("Could not open gift file!");
                    let mut bw = std::io::BufWriter::new(file);
                    let bytes = include_bytes!("shark.jpg");
                    bw.write_all(bytes).expect("Could not write gift data!");
                }

                true
            },
            None => false
        };

        if !success {
            println!("No gift for u!");
        }

        fcd.destroy();

        window.close();
    }));


    window.show_all();
}

fn main() {  
    let uiapp = gtk::Application::new(Some("pw.sharky.rust.test.gtk"),
                                      gio::ApplicationFlags::FLAGS_NONE)
        .expect("Application::new failed");
    
        uiapp.connect_activate(|app| {
            build_ui(app);
    });
    uiapp.run(&env::args().collect::<Vec<_>>());
}
